Zerkalo readme from Prool

This is Zerkalo - multiusers dungeon (MUD) engine (MUD setver).
Zerkalo is derivative of Byliny (Bylins) MUD.
Byliny is derivative of Circle MUD.

Language of Zerkalo and Byliny is Russian.

---

Зеркало. Текст от Пруля.

Зеркало это форк Былин, а Былины это потомок старой кодовой базы CircleMUD.
CircleMUD англоязычная база, а Былины и Зеркало построены на русском языке и
тематикой приключений основаны на старых русских сказках и былинах.

Отличия Зеркала от оригинальных Былин весьма небольшие, в первую очередь направленные
на оказуаливание, то есть упрощение игры, потому как в оригинальным Былинах игра
довольно хардкорна (трудна), хотя и там в последнее время убирают самые суровые
элементы игры, например плату за постой.

---

Как самому собрать Зеркало (или Былины)

Смотри оригинальный readme.markdown. Без проблем собирается в Ubuntu 64 bit или в Windows 64 bit
в средах cygwin или WSL.

Кратко:

Распаковать

Войти в каталог мада (тот, в котором данный файл и файл CMakeLists.txt)

mkdir build

cp cmake1.sh build

cd build

./cmake1.sh

make

На данном этапе может оказаться, что не хватает каких-то библиотек. Тогда читайте оригинальный readme.markdown.

---

Также я смог собрать этот код в Windows 7 32 bit и macOS Catalina.

---

Как запустить

Каталог bin.win надо переименовать в bin.

Далее надо удалить оригинальный каталог lib, а каталог lib.prool переименовать в lib.
Далее надо исполняемый файл circle (circle.exe для Windows), переместить из каталога build, где
он создастся при успешной компиляции, в каталог bin.

Файлы winprep и renamelib запускать не надо, так как для упрощения процесса я сам все уже приготовил в виде
вышупомянутого каталога lib.prool.

Затем надо набрать bin/circle (находясь в том каталоге, в котором находится данный файл и подкалог bin).
И мад запустится на порту 4000. Запустится мад с заставкой оригинальных Былин, двоичным кодом Зеркала
(который, как я уже говорил, суть слегка модифицированные Былины) и тремя начальными зонами.

Примечание: для сборки в среде 32-разрядного cygwin (то есть в 32-разрядных Виндах) надо перед тем, как делать
make, раскомментировать строку

//#define CYGWIN32

расположенную в начале файла db.cpp

Двоичный файл, полученный внутри cygwin, будет запускаться только из среды cygwin. Чтобы запускать его из Виндовс, придется
найти в положить в каталог bin нужные для работы dll файлы (в количестве около пяти). Какие файлы нужны, скажет сам бинарник,
при запуске, когда выдаст сообщени об ошибке типа "Не могу запуститься, ищу файл cygwin.dll). Все эти файлы берутся из
каталога c:/cygwin/bin

---

Пруль, 2021 год, ноябрь, Харьков, эпоха коронавирусной эпидемии.
